﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Canteen.FtpClient
{
    public class Client
    {
        private readonly string url;
        private readonly string login;
        private readonly string password;

        public Client(string url, string login, string password)
        {
            this.url = url;
            this.login = login;
            this.password = password;
        }

        private FtpWebRequest GetRequest(string method)
        {
            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(url);
            ftpRequest.Method = method;
            ftpRequest.Credentials = new NetworkCredential(login, password);

            return ftpRequest;
        }

        public string[] GetListFiles()
        {
            FtpWebResponse ftpResponse;
            try
            {
                ftpResponse = (FtpWebResponse)GetRequest(WebRequestMethods.Ftp
                    .ListDirectory).GetResponse();
            }
            catch (WebException webExcepption)
            {
                throw webExcepption;
            }
            catch (InvalidOperationException operationException)
            {
                throw operationException;
            }

            try
            {
                using (StreamReader stream = new StreamReader(ftpResponse.GetResponseStream()))
                {
                    string response = stream.ReadToEnd();

                    response = response
                        .Replace("\r", string.Empty);

                    return response
                        .Split("\n")
                        .Select(fileName => fileName)
                        .Where(fileName => fileName != string.Empty)
                        .ToArray();
                }
            }
            catch (IOException ioExcpetion)
            {
                throw ioExcpetion;
            }
            catch (ArgumentNullException argumentNullException)
            {
                throw argumentNullException;
            }
            catch (ArgumentException argumentException)
            {
                throw argumentException;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// 
        /// </returns>
        /// <exception cref="WebException">
        /// 
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// 
        /// </exception>
        public FileStream ReadFile()
        {
            FtpWebResponse ftpResponse;
            try
            {
                ftpResponse = (FtpWebResponse)GetRequest(WebRequestMethods.Ftp.DownloadFile)
                    .GetResponse();
            }
            catch (WebException webExcepption)
            {
                throw webExcepption;
            }
            catch (InvalidOperationException operationException)
            {
                throw operationException;
            }

            try
            {

                FileStream fileStream = new FileStream("temp.xls", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

                byte[] buffer = new byte[64];
                int size = 0;

                while ((size = ftpResponse.GetResponseStream().Read(buffer, 0, buffer.Length)) > 0)
                    fileStream.Write(buffer, 0, size);

                return fileStream;
            }
            catch (InvalidOperationException operationException)
            {
                throw operationException;
            }
        }

        public bool DeleteFile(string fileName)
        {
            try
            {
                string[] alreadyExistFiles = GetListFiles();
                if (alreadyExistFiles.Contains(fileName))
                {
                    FtpWebResponse ftpResponse = (FtpWebResponse)GetRequest(WebRequestMethods.Ftp.DeleteFile).GetResponse();
                    return ftpResponse.StatusCode == FtpStatusCode.FileActionOK;
                }

                return false;
            }
            catch { return false; }
        }

        public bool CreateFiles(string[] fileNames)
        {
            try
            {
                string[] alreadyExistFiles = GetListFiles();
                foreach (string fileName in fileNames)
                {
                    if (!DeleteFile(fileName)) return false;
                }

                return true;
            }
            catch { return false; }
        }

        public bool CreateFile(byte[] data)
        {
            try
            {
                FtpWebRequest ftpRequest = GetRequest(WebRequestMethods.Ftp.UploadFile);
                using(Stream ftpStream = ftpRequest.GetRequestStream())
                {
                    ftpRequest.ContentLength = data.Length;
                    ftpStream.Write(data, 0, data.Length);
                    ftpStream.Flush();
                }
                return true;
            }
            catch(Exception e)
            {
                int a = 1;
                return false;
            }
        }
    }
}
