﻿using Canteen.Logger;
using Canteen.Models.MainDatabase;
using Canteen.Models.DatabaseTemp;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;

namespace Canteen
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionTempDatabaseString = Configuration.GetConnectionString("DatabaseTempConnection");
            services.AddDbContext<DatabaseTempContext>(options => options.UseSqlite(connectionTempDatabaseString));

            string connectionDatabaseContext = Configuration.GetConnectionString("DatabaseConnection");
            services.AddDbContext<MainDatabaseContext>(options => options.UseSqlite(connectionDatabaseContext));

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddFile(Path.Combine(env.ContentRootPath, "log"));

            if (env.IsDevelopment())
            {
                loggerFactory.AddConsole(LogLevel.Trace);
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            ILogger logger = loggerFactory.CreateLogger("FileLogger");

            app.Run(async (context) =>
            {
                logger.LogInformation("request: ", context.Request.Path);
                await context.Response.WriteAsync("");               
            });
        }
    }
}
