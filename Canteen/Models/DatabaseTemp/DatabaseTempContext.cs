﻿using Microsoft.EntityFrameworkCore;

namespace Canteen.Models.DatabaseTemp
{
    public class DatabaseTempContext : DbContext 
    {
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        public DatabaseTempContext(DbContextOptions<DatabaseTempContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Department>()
                .HasMany(department => department.Bids)
                .WithOne(bid => bid.Department)
                .HasForeignKey(bid => bid.DepartmentId);                

            builder.Entity<Bid>()
                .HasMany(bid => bid.Tickets)
                .WithOne(ticket => ticket.Bid)
                .HasForeignKey(ticket => ticket.BidId);
        }

    }
}
