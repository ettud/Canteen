﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Canteen.Models.DatabaseTemp
{
    public class Ticket
    {
        public int Id { get; set; }
        public int TypeMenu { get; set; }

        public string DayOfWeek { get; set; }
        public string Date { get; set; }

        public int BidId { get; set; }
        [ForeignKey("BidId")]
        public Bid Bid { get; set; }

        public override bool Equals(object obj)
        {
            try
            {
                Ticket ticket = obj as Ticket;

                if (ReferenceEquals(this, ticket)) return true;

                return ticket.Date == this.Date;
            }
            catch { return false; }
        }
    }
}