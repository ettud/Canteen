﻿using System.Collections.Generic;
using System.Linq;

namespace Canteen.Models.DatabaseTemp
{
    public class Department
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<Bid> Bids { get; set; }

        public int GetCountBid(int typeMenu, string day) => Bids
                .Select(bid => bid.GetCountMenu(typeMenu, day))
                .Sum();

        public int GetCountBid(int typeMenu) => Bids
               .Select(bid => bid.GetCountMenu(typeMenu))
               .Sum();

        public Department()
        {
            Id = 0;
            Title = string.Empty;
            Bids = new List<Bid>();
        }

        public void AddBid(Bid bid)
        {
            if (bid is null) return;

            Bids.Add(bid);
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (ReferenceEquals(this, obj)) return true;

                return Title.Equals((obj as Department).Title);
            }
            catch
            {
                return false;
            }
        }
    }
}
