﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Canteen.Models.DatabaseTemp
{
    public class Bid
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public bool Paid { get; set; }
        public List<Ticket> Tickets { get; set; }

        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public Bid()
        {
            Id = 0;
            EmployeeName = string.Empty;
            Paid = false;
            Tickets = new List<Ticket>();
        }

        public string GetTypeMenuOfDayWeek(string dayOfWeek)
        {
            Ticket ticketOfDayWeek = Tickets
                .FirstOrDefault(ticket => ticket.DayOfWeek == dayOfWeek);

            return ticketOfDayWeek is null ? "-" : (ticketOfDayWeek.TypeMenu == 0 ? "" : ticketOfDayWeek.TypeMenu.ToString());
        }

        public int GetCountMenu(int typeMenu, string day) => Tickets
            .Select(ticket => ticket)
            .Where(ticket => ticket.TypeMenu == typeMenu && ticket.DayOfWeek == day)
            .Count();

        public int GetCountMenu(int typeMenu) => Tickets
            .Select(ticket => ticket)
            .Where(ticket => ticket.TypeMenu == typeMenu)
            .Count();

        public void AddTicket(Ticket ticket)
        {
            if (ticket is null) return;

            Tickets.Add(ticket);
        }

        public bool ContainsTicket(Ticket ticket)
        {
            if (ticket is null) return false;

            return Tickets.Contains(ticket);
        }
    }
}
