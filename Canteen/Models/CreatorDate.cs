﻿using System;

namespace Canteen.Models
{
    public class CreatorDate
    {
        public static DateTime CreateNext(DateTime date)
        {
            int day = (int)date.DayOfWeek;
            int difference = 0;

            if (day == 0)
                difference++;
            else
                difference = 8 - day;

            return date.AddDays(difference);
        }

        public static string GetDay(int day)
        {
            switch (day)
            {
                case 0: return "Понедельник";
                case 1: return "Вторник";
                case 2: return "Среда";
                case 3: return "Четверг";
                case 4: return "Пятница";
                default: return null;
            }
        }
    }
}
