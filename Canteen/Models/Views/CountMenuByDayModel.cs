﻿namespace Canteen.Models
{
    public class CountMenuByDayModel
    {
        public string Day { get; set; }
        public int TypeMenu { get; set; }
        public int Count { get; set; }
    }
}
