﻿using Canteen.Models.DatabaseTemp;
using System.Collections.Generic;

namespace Canteen.Models
{
    public class BidsViewModel
    {
        public IEnumerable<Bid> Bids { get; set; }
        public IEnumerable<Bid> NotPaidBids { get; set; }
    }
}
