﻿namespace Canteen.Models
{
    public class TicketModelView
    {
        public int TypeMenu { get; set; }
        public string Date { get; set; }
        public string EmployeeName { get; set; }
    }
}
