﻿namespace Canteen.Models
{
    public class TicketsPrintModel
    {
        public string Department { get; set; }
        public string[] EmployeeNames { get; set; }
    }
}
