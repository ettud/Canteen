﻿using Canteen.Models.DatabaseTemp;
using System.Collections.Generic;

namespace Canteen.Models
{
    public class StatisticModel
    {
        public List<Department> Departments { get; set; }
        public Dictionary<int,int> Menu { get; set; }
        public List<CountMenuByDayModel> CountMenuByDayModels { get; set; }
    }
}
