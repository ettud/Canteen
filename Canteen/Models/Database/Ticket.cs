﻿using System;

namespace Canteen.Models.Database
{
    public class Ticket
    {
        public int Id { get; set; }
        public int TypeMenu { get; set; }
        public string EmployeeName { get; set; }
        public DateTime Date { get; set; }
        public bool Paid { get; set; }
    }
}
