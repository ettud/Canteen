﻿namespace Canteen.Models.Database
{
    public class Department
    {
        public int Id { get; set; }
        public string Title { get; set; }

    }
}
