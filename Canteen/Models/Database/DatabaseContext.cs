﻿using Canteen.Models.DatabaseTemp;
using Microsoft.EntityFrameworkCore;

namespace Canteen.Models.MainDatabase
{
    public class MainDatabaseContext : DbContext
    {
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        public MainDatabaseContext(DbContextOptions<MainDatabaseContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Department>()
                .HasMany(department => department.Bids)
                .WithOne(bid => bid.Department)
                .HasForeignKey(bid => bid.DepartmentId);

            builder.Entity<Bid>()
                .HasMany(bid => bid.Tickets)
                .WithOne(ticket => ticket.Bid)
                .HasForeignKey(ticket => ticket.BidId);
        }
    }
}
