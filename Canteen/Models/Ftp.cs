﻿namespace Canteen.Models
{
    public class Ftp
    {
        public string Url { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
