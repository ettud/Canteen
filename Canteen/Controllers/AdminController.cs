﻿using System.Collections.Generic;
using System.Linq;
using Canteen.FtpClient;
using Canteen.Models.DatabaseTemp;
using Microsoft.AspNetCore.Mvc;

namespace Canteen.Controllers
{
    public class AdminController : Controller
    {
        private readonly DatabaseTempContext database;

        public AdminController(DatabaseTempContext database)
        {
            this.database = database;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult EditTitleDepartments()
        {
            List<Department> departments = database
                .Departments
                .ToList();

            foreach (Department department in departments)
            {
                string departmentTitle = department.Title;
                try
                {
                    departmentTitle = departmentTitle.Remove(departmentTitle.IndexOf('\n'));
                    departmentTitle = departmentTitle.Remove(departmentTitle.IndexOf('\t'));
                    departmentTitle = departmentTitle.Remove(departmentTitle.IndexOf('\r'));
                }
                catch { continue; }
            }

           return RedirectToAction("Index"); 
        }


        public IActionResult Test()
        {
            Client client = new Client("ftp://ftp-scan", "Canteen", "98412");
            var res = client.GetListFiles();

            return Ok();
        }
    }
}