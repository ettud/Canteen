﻿using Canteen.Models;
using Canteen.Models.DatabaseTemp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Canteen.Controllers
{
    public class CafeController : Controller
    {
        private DatabaseTempContext database;

        public CafeController(DatabaseTempContext database)
        {
            this.database = database;
        }

        public IActionResult Index()
        {
            try
            {
                List<Department> departments = database
                    .Departments
                    .Include(department => department.Bids)
                    .ThenInclude(bid => bid.Tickets)?
                    .ToList();

                List<int> typesMenu = database
                    .Tickets
                    .Select(ticket => ticket.TypeMenu)
                    .Where(type => type > 0)?                    
                    .ToList()
                    .Distinct()
                    .OrderBy(type => type)
                    .ToList();

                List<CountMenuByDayModel> countsMenu = new List<CountMenuByDayModel>();

                for (int numberDay = 0; numberDay < 5; numberDay++)
                {
                    string day = CreatorDate.GetDay(numberDay);
                    foreach (int typeMenu in typesMenu)
                    {
                        int count = 0;
                        foreach (Department department in departments)
                        {
                            int tempCount = department.GetCountBid(typeMenu, day);
                            count += tempCount;
                        }

                        countsMenu.Add(new CountMenuByDayModel
                        {
                            Day = day,
                            TypeMenu = typeMenu,
                            Count = count
                        });
                    }
                }


                Dictionary<int, int> menu = new Dictionary<int, int>();

                foreach (int typeMenu in typesMenu)
                {
                    int count = departments
                                .Select(d => d.GetCountBid(typeMenu))
                                .Sum();

                    menu.Add(typeMenu, count);
                }

                StatisticModel model = new StatisticModel
                {
                    Departments = departments,
                    Menu = menu,
                    CountMenuByDayModels = countsMenu
                };

                return View("Index", model);
            }
            catch (Exception e)
            {
                return View("Views/Shared/ErrorView.cshtml");
            }
        }

        public IActionResult DownloadReport()
        {
            return Ok();
        }
    }
}