﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Canteen.File;
using Canteen.Models;
using Canteen.Models.MainDatabase;
using Canteen.Models.DatabaseTemp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Canteen.FtpClient;
using System.IO;
using System.Net;

namespace Canteen.Controllers
{
    public class OperatorController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly DatabaseTempContext tempDatabase;
        private readonly MainDatabaseContext mainDatabase;

        private readonly string NAME_SHEET = "Лист1";

        public OperatorController(DatabaseTempContext tempDatabase,
                                  MainDatabaseContext mainDatabase,
                                  IConfiguration configuration)
        {
            this.tempDatabase  = tempDatabase;
            this.mainDatabase  = mainDatabase;
            this.configuration = configuration;
        }

        public async Task<IActionResult> CreateTeamplate()
        {
            try
            {
                try
                {
                    await ClearTempDatabase();
                }
                catch(Exception e)
                {
                    string error = $"Ошибка очистки временной базы данных\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                DateTime date = CreatorDate
                    .CreateNext(DateTime.Now);

                string[] departments = tempDatabase
                    .Departments
                    .Select(department => department.Title)
                    .ToArray();

                FileCreator fileCreator;
                string passwordFile;
                Ftp ftp;
                try
                {
                    fileCreator = new FileCreator();
                    passwordFile = configuration
                        .GetSection("PasswordFile")
                        .Get<string>();                    

                    ftp = configuration.GetSection("Ftp").Get<Ftp>();

                    if(ftp is null)
                    {
                        string error = $"ошибка чтения настроек";
                        return View("Views/Shared/ErrorView.cshtml", error);
                    }
                }
                catch(Exception e)
                {
                    string error = $"ошибка чтения настроек\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                foreach (string department in departments)
                {
                    try
                    {
                        string fileName = $"Заявка {department}.xls";
                        byte[] data = fileCreator.CreateTeamplate(fileName, NAME_SHEET, date, passwordFile);

                        string url = $"ftp://{ftp.Url}/{fileName}";

                        Client client = new Client(url, ftp.Login, ftp.Password);
                        var a = client.DeleteFile(fileName);
                        var res = client.CreateFile(data);
                    }
                    catch { continue; }
                }

                return RedirectToAction("Index", "Cafe");
            }
            catch (Exception e)
            {
             return View("Views/Shared/ErrorView.cshtml");
            }
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                string[] departments = tempDatabase
                    .Departments
                    .Select(department => department.Title)
                    .OrderBy(departmentName => departmentName)
                    .ToArray();

                return View("BidsView", departments);
            }
            catch(Exception e)
            {
                string error = $"Ошибка чтения данных\n{e.Message}";
                return View("Views/Shared/ErrorView.cshtml", error);
            }
        }

        [HttpGet]
        public async Task<IActionResult> ReadBids(string titleDepartment)
        {
            try
            {
                string fileName = $"Заявка {titleDepartment}.xls";

                Ftp ftp;
                try
                {
                    ftp = configuration.GetSection("Ftp").Get<Ftp>();

                    if(ftp is null)
                    {
                        string error = $"ошибка чтения настроек";
                        return View("Views/Shared/ErrorView.cshtml", error);
                    }                    
                }
                catch(Exception e)
                {
                    string error = $"ошибка чтения настроек\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }
                        
                Client client;
                try
                {
                    string url = $"ftp://{ftp.Url}/{fileName}";
                    client = new Client(url, ftp.Login, ftp.Password);
                }
                catch(Exception e)
                {
                    string error = $"ошибка подключения к ftp серверу\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                FileStream stream;
                try
                {
                    stream = client.ReadFile();
                }
                catch(Exception e)
                {
                    string error = $"Ошибка создания временного файла\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                List<Bid> bids;
                try
                {
                    FileViewer viewer = new FileViewer();

                    bids = viewer
                        .ReadBids(stream, NAME_SHEET)
                        .ToList();

                    stream.Close();
                }
                catch(Exception e)
                {
                    stream.Close();
                    string error = $"Ошибка чтения временного файла\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }


                foreach (Bid bid in bids)
                {
                    Department department;
                    Bid bidInDatabaseTemp;
                    try
                    {
                        department = await tempDatabase
                            .Departments
                            .Include(d => d.Bids)
                            .ThenInclude(b => b.Tickets)
                            .FirstOrDefaultAsync(d => d.Title == titleDepartment);

                        bidInDatabaseTemp = department
                            .Bids
                            .LastOrDefault(b => b.EmployeeName == bid.EmployeeName);
                    }
                    catch(Exception e)
                    {
                        string error = $"Ошибка чтения заявок отдела{titleDepartment}\n{e.Message}";
                        return View("Views/Shared/ErrorView.cshtml", error);
                    }

                    if (bidInDatabaseTemp is null) continue;

                    foreach (Ticket ticket in bidInDatabaseTemp.Tickets)
                    {
                        if (bid.Tickets.Contains(ticket))
                        {
                            bid.Paid = true;
                            break;
                        }
                    }
                }

                List<Bid> paidBids = bids
                    .Select(bid => bid)
                    .Where(bid => bid.Paid)
                    .ToList();

                if (paidBids is null)
                    paidBids = new List<Bid>();

                List<Bid> notPaidBids = bids
                    .Select(bid => bid)
                    .Where(bid => !bid.Paid)
                    .ToList();

                if (notPaidBids is null)
                    notPaidBids = new List<Bid>();

                BidsViewModel bidsModel = new BidsViewModel
                {
                    Bids = paidBids,
                    NotPaidBids = notPaidBids
                };

                return PartialView("BidsTable", bidsModel);
            }
            catch (WebException webException)
            {
                return View("Views/Shared/ErrorView.cshtml", "Файл открыт другим пользователем");
            }
            catch (Exception e)
            {
                return View("Views/Shared/ErrorView.cshtml");
            }
        }

        [HttpPost]
        public async Task<IActionResult> PrintTickets([FromBody] TicketsPrintModel model)
        {
            try
            {
                string fileName = $"Заявка {model.Department}.xls";

                Ftp ftp;
                try
                {
                    ftp = configuration.GetSection("Ftp").Get<Ftp>();
                }
                catch(Exception e)
                {
                    string error = $"ошибка чтения настроек\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                Client client;
                try
                {
                    string url = $"ftp://{ftp.Url}/{fileName}";
                    client = new Client(url, ftp.Login, ftp.Password);
                }
                catch(Exception e)
                {
                    string error = $"ошибка подключения к ftp серверу\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                FileStream stream;
                try
                {
                    stream = client.ReadFile();
                }
                catch (Exception e)
                {
                    string error = $"Ошибка чтения временного файла\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                
                List<Bid> bids;
                try
                {
                    FileViewer viewer = new FileViewer();
                    bids = viewer
                        .ReadBids(stream, NAME_SHEET)
                        .Select(bid => bid)
                        .Where(bid => bid.Tickets.Count > 0)
                        .ToList();

                    stream.Close();
                }
                catch(Exception e)
                {
                    stream.Close();
                    string error = $"Ошибка чтения временного файла\n{e.Message}";
                    return View("Views/Shared/ErrorView.cshtml", error);
                }

                List<Bid> bidsForPrint = new List<Bid>();
                foreach (string employeeName in model.EmployeeNames)
                {
                    string aa = employeeName;
                    Bid bid = bids
                        .FirstOrDefault(b => b.EmployeeName.Equals(employeeName, StringComparison.InvariantCultureIgnoreCase));

                    if (bid is null) continue;


                    bid.Paid = true;
                    bidsForPrint.Add(bid);

                    Department department = await tempDatabase
                        .Departments
                        .Include(d => d.Bids)
                        .ThenInclude(b => b.Tickets)
                        .FirstOrDefaultAsync(d => d.Title == model.Department);

                    Bid bidInDatabaseTemp = department
                        .Bids
                        .LastOrDefault(b => b.EmployeeName == bid.EmployeeName);

                    if (bidInDatabaseTemp is null)
                    {
                        department
                            .AddBid(bid);

                        tempDatabase
                            .Departments
                            .Update(department);

                        continue;
                    }

                    bool isContains = false;
                    foreach (Ticket ticket in bidInDatabaseTemp.Tickets)
                    {
                        if (bid.ContainsTicket(ticket))
                        {
                            isContains = true;
                            bid.Paid = true;
                            break;
                        }
                    }

                    if (!isContains)
                    {
                        department
                            .AddBid(bid);
                    }

                    tempDatabase
                        .Departments
                        .Update(department);
                }


                await tempDatabase
                    .SaveChangesAsync();

                return View("TicketsView", bidsForPrint);
            }
            catch (Exception e)
            {
                return View("Views/Shared/ErrorView.cshtml");
            }
        }

        public IActionResult WindowForPrint() => View();

        private async Task ClearTempDatabase()
        {
            List<Department> tempDepartments = tempDatabase
                    .Departments
                    .Include(department => department.Bids)
                        .ThenInclude(bid => bid.Tickets)
                    .ToList();

            List<Department> mainDepartments = mainDatabase
                .Departments
                .ToList();

            foreach (Department tempDepartment in tempDepartments)
            {
               
                if (mainDepartments.Contains(tempDepartment))
                {
                    Department mainDepartment = mainDepartments[mainDepartments.IndexOf(tempDepartment)];

                    List<Bid> bids = tempDepartment.Bids.ToList();
                    tempDatabase.RemoveRange(bids);
                    await tempDatabase.SaveChangesAsync();

                    bids.ForEach(bid =>
                    {
                        bid.Id = 0;
                        bid.DepartmentId = 0;
                        bid.Department = null;

                        bid.Tickets.ForEach(ticket =>
                        {
                            ticket.Id = 0;
                            ticket.BidId = 0;
                            ticket.Bid = null;
                        });
                    });

                    mainDepartment.Bids.AddRange(bids);
                }
                else
                {
                    Department newDepartment = tempDepartment;
                    mainDatabase
                        .Departments
                        .Add(tempDepartment);
                }                    
            }

            try
            {
                await tempDatabase
                .SaveChangesAsync();
                await mainDatabase
                    .SaveChangesAsync();
            }
            catch(Exception e)
            {
                return;
            }
        }
    }
}