﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Canteen
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            string url = args.Length == 0 ? string.Empty : args[0];
            return WebHost.CreateDefaultBuilder(args)
                .UseUrls(url)
                .UseStartup<Startup>();
        }            
    }
}
