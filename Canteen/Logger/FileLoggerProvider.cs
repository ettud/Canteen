﻿using Microsoft.Extensions.Logging;

namespace Canteen.Logger
{
    public class FileLoggerProvider : ILoggerProvider {

        private readonly string filePath;

        public FileLoggerProvider(string filePath)
        {
            this.filePath = filePath;
        }        

        public ILogger CreateLogger(string categoryName) => new FileLogger(filePath);

        public void Dispose() { }
    }
}
