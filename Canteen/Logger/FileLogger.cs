﻿using System;
using Microsoft.Extensions.Logging;

namespace Canteen.Logger
{
    public class FileLogger : ILogger
    {
        private string filePath;
        private object _lock;

        public FileLogger(string filePath)
        {
            this.filePath = filePath;
            _lock = new object();
        }


        public IDisposable BeginScope<TState>(TState state) => null;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, 
            Exception exception, Func<TState, Exception, string> formatter)
        {
            if(formatter != null)
            {
                lock(_lock)
                {
                    string content = $"{DateTime.Now.ToString()} - {formatter(state, exception)} {Environment.NewLine}{Environment.NewLine}{Environment.NewLine}";
                    System.IO.File.AppendAllText(filePath, content);
                }
            }
        }
    }
}
