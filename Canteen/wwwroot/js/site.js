﻿function showTable() {
    var department = $('#list-departments').val();
    var url = host + readBitsRoute + '?titleDepartment=' + department;
    var url1 = 'http://localhost:5000/bids/ReadBids?titleDepartment=ОМИ';

    getRequest(url, putTable);
}

function putTable(viewTable) {
    var container = $('.table-container');
    container.html(viewTable);
}

function printTickets() {
    window.print();
    windows.reload();
}

function getRequest(url, successFunction) {
    $.ajax({
        accepts: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        host: host,
        url: url,
        success: function (viewTable) {
            successFunction(viewTable);
        },
        error: function (error) {
            var a = error;
        } 
    });
}

function postRequest(url, data) {

    $.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(data),
        success: function (view) {
            openNewWindow(view);
        },
        error: function (error) {
            var a = error;
        },
        contentType: "application/json; charset=utf-8"
    });
}

function openNewWindow(view) {
    var url = host + 'operator';
    var newWin = window.open(url, '', 'left=50,top=50,width=800,height=640,toolbar=0,scrollbars=1,status=0');

    newWin.onload = function () {
        newWin.document.body.innerHTML = view;
        newWin.print();
        newWin.close();
    };
}

function selectTicketsForPrint() {
    var checkboxs = document.all['checkbox']; 
    var employeeNames = document.all['employeeName'];

    var checkedEmployees = [];
    var count = 0;

    var department = $('#list-departments').val();

    
    var url = host + printTicketsRoute;

    if (checkboxs.length === undefined) {        

        if (checkboxs.checked) {
            checkedEmployees[count] = employeeName.innerText;

            var soloModel = {
                Department: department,
                employeeNames: checkedEmployees
            };
            postRequest(url, soloModel);
        }              

        return;
    }

    for (var numberRow = 0; numberRow < checkboxs.length; numberRow++) {
        if (checkboxs[numberRow].checked) {
            checkedEmployees[count] = employeeNames[numberRow].innerText;
            count++;
        }
    }

    var model = {
        Department: department,
        employeeNames: checkedEmployees
    };
    postRequest(url, model);
}

function calculateCost() {
    var checkboxs = document.all['checkbox'];

    if (checkboxs === undefined) return;

    var rows = getRows();
    var count = 0;
    var finalCost = 0;

    if (checkboxs.length === undefined) {
        if (checkboxs.checked) {
            finalCost += calculateRow(rows[0]);
            count++;
        }
    } else {
        for (var numberRow = 0; numberRow < checkboxs.length; numberRow++) {
            if (checkboxs[numberRow].checked) {
                finalCost += calculateRow(rows[numberRow]);
                count++;
            }
        }
    }

    var cost = $('#cost');
    var resultCost = finalCost + '₽';
    cost.text(resultCost);
}

function getRows() {
    var tables = $('.table');

    var rows = [];
    var countRows = 0;

    for (var numberTable = 0; numberTable < tables.length; numberTable++) {
        var currentTable = tables[numberTable];

        if (currentTable.children.length < 2) continue;

        var rowsOfCurrentTable = currentTable.children[1].children;
        for (var numberRow = 0; numberRow < rowsOfCurrentTable.length; numberRow++) {
            rows[countRows] = rowsOfCurrentTable[numberRow];
            countRows++;
        }
    }

    return rows;
}

function calculateRow(tableRow) {
    var cells = tableRow.children;

    var costOfRow = 0;

    if (cells.length < 7) return undefined;

    for (var numberCell = 2; numberCell < cells.length; numberCell++) {

        var text = cells[numberCell].text;
        if (cells[numberCell].innerText !== '-') {
            costOfRow += 100;
        } 
    }

    return costOfRow;
}