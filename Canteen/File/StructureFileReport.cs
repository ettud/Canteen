﻿namespace Canteen.File
{
    public class StructureFileReport
    {
        public const int COUNT_COLUMN = 2;

        public const int MAX_COLUMNS = 6;

        public const short COLUMN_WIDTH = 7000;
        public const short HEAD_COLUMN_HEIGHT = 1000;
        public const short DATA_COLUMN_HEIGHT = 500;

        public const int ROW_HEADER = 0;
        public const int ROW_DATA = 1;

        public const int COLUMN_DEPARTMENT_NAME = 0;
        public const int COLUMN_MONDAY = 1;
        public const int COLUMN_TUESDAY = 2;
        public const int COLUMN_WEDNESDAY = 3;
        public const int COLUMN_THURSDAY = 4;
        public const int COLUMN_FRIDAY = 5;
    }
}
