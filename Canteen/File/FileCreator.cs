﻿using Canteen.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Canteen.File
{
    public class FileCreator
    {
        private readonly string directory;

        public FileCreator()
        {

        }

        public FileCreator(string directory)
        {
            this.directory = directory;
        }

        public byte[] CreateTeamplate(string fileName, string sheetName, DateTime date, string password)
        {
            fileName = $"temp files/{fileName}";
            using (FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                IWorkbook excelDocument = new HSSFWorkbook();
                ISheet sheet = excelDocument.CreateSheet(sheetName);

                IFont font = excelDocument.CreateFont();
                font.Boldweight = (short)FontBoldWeight.Bold;

                ICellStyle headStyle = excelDocument.CreateCellStyle();
                headStyle.Alignment = HorizontalAlignment.Center;
                headStyle.VerticalAlignment = VerticalAlignment.Center;
                headStyle.SetFont(font);
                SetBorderCell(headStyle, BorderStyle.Thin);

                ICellStyle dataStyle = excelDocument.CreateCellStyle();
                dataStyle.Alignment = HorizontalAlignment.Center;
                dataStyle.VerticalAlignment = VerticalAlignment.Center;
                dataStyle.IsLocked = false;
                SetBorderCell(dataStyle, BorderStyle.Thin);

                CreateHeadTicket(sheet, headStyle, "ФИО", date);
                CreateCellForEdit(sheet, dataStyle);

                sheet.ProtectSheet(password);

                excelDocument.Write(stream);

            };

            byte[] data;
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
            {
                
                data = new byte[fileStream.Length];
                fileStream.Read(data, 0, data.Length);
            }

            return data;
        }

        //public HSSFWorkbook CreateReportFile(string fileName, string nameSheet, DateTime date) 
        //{
        //    string tempFileName = $"reports/report-{date.Day}.{date.Month}.{date.Year} {fileName}";

        //    HSSFWorkbook workbook = new HSSFWorkbook();
        //    HSSFSheet sheet = workbook.CreateSheet(nameSheet) as HSSFSheet;


        //}

        private void CreateHeadTicket(ISheet sheet, ICellStyle headStyle, string firstColumn, DateTime date)
        {
            IRow firstRow = sheet.CreateRow(StructureFileTeamplate.ROW_HEADER);
            firstRow.Height = StructureFileTeamplate.HEAD_COLUMN_HEIGHT;

            List<ICell> cells = CreateCells(firstRow, StructureFileTeamplate.ROW_HEADER, StructureFileTeamplate.MAX_EDITING_COLUMNS, headStyle)
                .ToList();

            cells[StructureFileTeamplate.COLUMN_FULLNAME_EMPLOYEE]?.SetCellValue(firstColumn);

            cells[StructureFileTeamplate.COLUMN_MONDAY]?.SetCellValue($"Понедельник {date.Day}.{date.Month}.{date.Year}");
            date = date.AddDays(1);

            cells[StructureFileTeamplate.COLUMN_TUESDAY]?.SetCellValue($"Вторник {date.Day}.{date.Month}.{date.Year}");
            date = date.AddDays(1);

            cells[StructureFileTeamplate.COLUMN_WEDNESDAY]?.SetCellValue($"Среда {date.Day}.{date.Month}.{date.Year}");
            date = date.AddDays(1);

            cells[StructureFileTeamplate.COLUMN_THURSDAY]?.SetCellValue($"Четверг {date.Day}.{date.Month}.{date.Year}");
            date = date.AddDays(1);

            cells[StructureFileTeamplate.COLUMN_FRIDAY]?.SetCellValue($"Пятница {date.Day}.{date.Month}.{date.Year}");

            for (int numberColumn = 0; numberColumn < StructureFileTeamplate.MAX_EDITING_COLUMNS; numberColumn++)
                sheet.SetColumnWidth(numberColumn, StructureFileTeamplate.COLUMN_WIDTH);
        }

        private void CreateCellForEdit(ISheet sheet, ICellStyle dataStyle)
        {
            List<IRow> editingRows = CreteRows(sheet, StructureFileTeamplate.ROW_DATA, StructureFileTeamplate.MAX_EDITING_ROWS)
                .ToList();
            foreach (IRow row in editingRows)
            {
                row.Height = StructureFileTeamplate.DATA_COLUMN_HEIGHT;
                List<ICell> editingCells = CreateCells(row, 0, StructureFileTeamplate.MAX_EDITING_COLUMNS, dataStyle)
                    .ToList();

                CellRangeAddressList cellRange = new CellRangeAddressList(StructureFileTeamplate.ROW_DATA, StructureFileTeamplate.MAX_EDITING_ROWS, 
                    StructureFileTeamplate.COLUMN_MONDAY, StructureFileTeamplate.COLUMN_FRIDAY);
                SetDropdownList(sheet, cellRange);
            }
        }        

        private IEnumerable<ICell> CreateCells(IRow row, int begin, int amount, ICellStyle style)
        {
            List<ICell> cells = new List<ICell>();

            for (int numberCell = begin; numberCell < amount; numberCell++)
            {
                ICell cell = row.CreateCell(numberCell);
                cell.CellStyle = style;
                cells.Add(cell);
            }

            return cells;
        }        

        private IEnumerable<IRow> CreteRows(ISheet sheet, int begin, int amount)
        {
            List<IRow> rows = new List<IRow>();

            for (int numberRow = begin; numberRow < amount; numberRow++)
                rows.Add(sheet.CreateRow(numberRow));

            return rows;
        }

        private void SetDropdownList(ISheet sheet, CellRangeAddressList addressList)
        {
            IDataValidationConstraint constraint = DVConstraint.CreateExplicitListConstraint(new string[] { TypeMenu.FIRST.ToString(),
                TypeMenu.SECOND.ToString() });
            IDataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
            dataValidation.CreateErrorBox("Неверное значение", "Пожалуйста введите значение от 1 до 2");
            sheet.AddValidationData(dataValidation);
        }

        private void SetBorderCell(ICellStyle style, BorderStyle borderStyle)
        {
            style.BorderLeft   = borderStyle;
            style.BorderTop    = borderStyle;
            style.BorderRight  = borderStyle;
            style.BorderBottom = borderStyle;
        }
    }
}
