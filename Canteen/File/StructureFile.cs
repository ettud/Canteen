﻿namespace Canteen.File
{
    public class StructureFile
    {
        public const int MAX_EDITING_ROWS    = 20;
        public const int MAX_EDITING_COLUMNS = 6;

        public const short COLUMN_WIDTH       = 7000;
        public const short HEAD_COLUMN_HEIGHT = 1000;
        public const short DATA_COLUMN_HEIGHT = 500;

        public const int ROW_HEADER = 0;
        public const int ROW_DATA   = 1;

        public const int COLUMN_FULLNAME_EMPLOYEE = 0;

        public const int COLUMN_MONDAY    = 1;
        public const int COLUMN_TUESDAY   = 2;
        public const int COLUMN_WEDNESDAY = 3;
        public const int COLUMN_THURSDAY  = 4;
        public const int COLUMN_FRIDAY    = 5;

        public const int COLUMN_TITLE_TYPE_MENU = 10;
        public const int COLUMN_FIRST_MENU      = 11;
        public const int COLUMN_SECOND_MENU     = 12;
    }
}
