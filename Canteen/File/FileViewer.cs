﻿using Canteen.Models.DatabaseTemp;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Canteen.File
{
    //доделать
    public class FileViewer
    {
        private readonly string directory;

        public FileViewer(string directory) => this.directory = directory;

        public FileViewer() { }

        public IEnumerable<string> FileList() => Directory.GetFiles(directory);

        public IEnumerable<Bid> ReadBids(string fileName, string nameSheet)
        {
            IWorkbook book = ReadBook(fileName);            

            ISheet sheet = book.GetSheet(nameSheet);

            if (sheet is null) return null;

            return ReadSheet(sheet);
        }

        public IEnumerable<Bid> ReadBids(FileStream stream, string nameSheet)
        {
            IWorkbook book = ReadBook(stream );

            ISheet sheet = book.GetSheet(nameSheet);

            if (sheet is null) return null;

            return ReadSheet(sheet);
        }

        public List<string> ReadDays(string fileName, string nameSheet)
        {
            try
            {
                IWorkbook book = ReadBook(fileName);
                ISheet sheet = book.GetSheet(nameSheet);

                IRow row = sheet.GetRow(StructureFileTeamplate.ROW_HEADER);

                List<string> dates = new List<string>();
                for (int numberColumn = StructureFileTeamplate.COLUMN_MONDAY; numberColumn < StructureFileTeamplate.COLUMN_FRIDAY; numberColumn++)
                {
                    string textColumn = row.GetCell(numberColumn)?.StringCellValue;
                    if (textColumn is null) return null;
                    dates
                        .Add(textColumn);
                }

                return dates;
            }
            catch
            {
                return null;
            }
        }
        
        private HSSFWorkbook ReadBook(Stream stream) => new HSSFWorkbook(stream);

        private HSSFWorkbook ReadBook(string fileName)
        {
            string fullFileName = $"{directory}\\{fileName}";
            using (FileStream fileStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                return new HSSFWorkbook(fileStream);
            }
        }

        private List<Bid> ReadSheet(ISheet sheet)
        {
            List<Bid> bids = new List<Bid>();
            for (int rowNumber = StructureFileTeamplate.ROW_DATA; rowNumber < StructureFileTeamplate.MAX_EDITING_ROWS; rowNumber++)
            {
                IRow row = sheet.GetRow(rowNumber);

                if (row is null) continue;

                ICell employeeNameCell = sheet
                    .GetRow(rowNumber)
                    .GetCell(StructureFileTeamplate.COLUMN_FULLNAME_EMPLOYEE);

                if (employeeNameCell is null ||
                    employeeNameCell.StringCellValue == string.Empty) continue; ;

                List<Ticket> tickets = new List<Ticket>();
                for (int cellNumber = StructureFileTeamplate.COLUMN_MONDAY; cellNumber <= StructureFileTeamplate.COLUMN_FRIDAY; cellNumber++)
                {
                    int typeMenu = (int)row.GetCell(cellNumber).NumericCellValue;
                    if (typeMenu == 0) continue;
                    ;
                    string valueDateCell = sheet.GetRow(StructureFileTeamplate.ROW_HEADER)
                        .GetCell(cellNumber)
                        .StringCellValue;

                    string dayOfWeek = valueDateCell
                        .Split(" ")
                        .FirstOrDefault();

                    string date = valueDateCell
                        .Split(" ")
                        .LastOrDefault();

                    Ticket ticket = new Ticket
                    {
                        TypeMenu = typeMenu,
                        DayOfWeek = dayOfWeek,
                        Date = date
                    };

                    tickets.Add(ticket);
                }

                string employeeName = employeeNameCell.StringCellValue;

                if(employeeName != null && employeeName.Length > 0)
                {
                    while (employeeName.First() == ' ' || employeeName.Last() == ' ')
                    {
                        employeeName = employeeName.First() == ' ' ? employeeName.Remove(employeeName.IndexOf(' '), 1) : employeeName;

                        if (employeeName == string.Empty) break;

                        employeeName = employeeName.Last() == ' ' ? employeeName.Remove(employeeName.LastIndexOf(' '), 1) : employeeName;

                        if (employeeName == string.Empty) break;
                    }   
                }
                

                bids.Add(new Bid
                {
                    EmployeeName = employeeName,
                    Tickets = tickets
                });
            }

            return bids;
        }

    }
}
